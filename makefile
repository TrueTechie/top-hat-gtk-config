# Directories
dot_DIR = .

# Files
dotfiles = $(wildcard $(dot_DIR)/DOT*)

# Destinations
skel_DEST = $(DESTDIR)/etc/skel
dot_DEST = $(skel_DEST)/


default: 
	$(error This makefile is just for installation/uninstallation for now!)

install:
	install -d $(skel_DEST)
	for d in $(dotfiles); do \
		install -m644 $$d $(dot_DEST)/$${d/DOT/.}; \
	done
uninstall:
	for d in $(dotfiles); do \
		rm -f $(dot_DEST)/$${d/DOT/.}; \
	done
